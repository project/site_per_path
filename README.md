# Site per path

This Drupal module provides an easy way to switch between multiple sites on 
given paths.

## Usage

This module is especially useful for multisite projects, where one installation
serves as main content deliverer. E.g. [Contentpool](https://www.drupal.org/project/contentpool)
projects.

### Configuration structure

Example of a yaml file, that redirects all administration pages to the contentpool:

     sites:
       - target_url: https://content.pool
         condition:
           negate: false
           pages: |
             /admin/*
             /node/*/edit

As there is no UI (yet), configuration must be altered either via editing and
importing config files or via drush command.
e.g. `drush config:edit site_per_path.settings`
    
## Credits

* Initial development by Arthur Lorenz, drunomics GmbH 
<arthur.lorenz@drunomics.com>
