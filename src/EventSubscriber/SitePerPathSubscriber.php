<?php

namespace Drupal\site_per_path\EventSubscriber;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class SitePerPathSubscriber.
 */
class SitePerPathSubscriber implements EventSubscriberInterface {

  /**
   * The condition manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $conditionManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * SitePerPathSubscriber constructor.
   *
   * @param \Drupal\Core\Condition\ConditionManager $conditionManager
   *   The condition manager.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(ConditionManager $conditionManager, ConfigFactory $configFactory, RequestStack $requestStack) {
    $this->conditionManager = $conditionManager;
    $this->configFactory = $configFactory;
    $this->requestStack = $requestStack;
  }

  /**
   * Checks whether request should be redirected to another site.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The response event.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function onKernelRequest(GetResponseEvent $event) {
    $sites = $this->configFactory->get('site_per_path.settings')->get('sites');

    // Do nothing if no config is set.
    if (!$sites) {
      return;
    }

    foreach ($sites as $site) {
      $request_path = $this->conditionManager->createInstance('request_path', $site['condition']);
      // RequestPath::evaluate() does not consider negation of matched paths.cd
      if ($request_path->evaluate() xor $request_path->isNegated()) {
        $redirect_url = $site['target_url'] . $this->requestStack->getCurrentRequest()->getPathInfo();

        // Don't redirect when already on correct site.
        if (strpos($this->requestStack->getCurrentRequest()->getUri(), $redirect_url) === FALSE) {
          $response = new TrustedRedirectResponse($redirect_url);
          $response->addCacheableDependency($this->configFactory->get('site_per_path.settings'));
          $event->setResponse($response);
          $event->stopPropagation();
        }
      }
    }
  }

  /**
   * Responds to kernel respones.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The event.
   */
  public function onKernelResponse(FilterResponseEvent $event) {
    $response = $event->getResponse();
    if ($response instanceof CacheableResponseInterface) {
      $response->addCacheableDependency((new CacheableMetadata())->addCacheContexts(['url.site']));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => ['onKernelRequest', 500],
      // Run before dynamic page cache which has prio 100.
      KernelEvents::RESPONSE => ['onKernelResponse', 500],
    ];
  }

}
